# space

<div align="center">
  <img src="https://cobox.cloud/src/svg/logo.svg">
</div>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Table of Contents

  - [About](#about)
  - [Install](#install)
  - [Usage](#usage)
  - [API](#api)
  - [Contributing](#contributing)
  - [License](#license)

## About
**CoBox** is an encrypted p2p file system and distributed back-up tool. [README](https://gitlab.com/coboxcoop/readme) provides a map of the project.

`Space` inherits from the `Replicator` class, and adds a `KappaDrive` instance with content encryption, enabling 'blind' replication for a regular Replicator class.

## Install
```
npm install @coboxcoop/space
```

## Usage
## API

`const Space = require('@coboxcoop/space')`

`var space = Space(storage, address, identity, opts)`

Returns a space object

- `storage` - path for storing feed data.  Defaults to a corestore instance
- `address` - the space key (which may be a compound key containing a symmetric encryption key)
- `identity` - the identity opening this space locally
- `opts` an options object

### `space.ready(callback)`

callback is called when the space is ready to use.

### `space.keys`

An object with properties `address` and `encryptionKey` as detailed below.

### `space.address`

The address or public key for the space

### `space.encryptionKey`

Random 32 byte key used to derive encryption key.

### `space.drive`

Your `KappaDrive` instance

### `space.state`

An interface to query your space drive changes, built with `kappa-view-query`.

### `space.log`

An additional hypercore with indexes attached for storing space-specific messages and writing queries. See [@coboxcoop/log](https://gitlab.com/coboxcoop/log)

### `space.swarm(opts)`

Swarm across either Hyperswarm, or Discovery Swarm, based on opts.

### `space.bytesUsed()`

Returns the cumulative size of all of the spaces feeds.  Cannot be run before space is ready. Note that this is only the size of the data in the feeds, in reality a little more disk space is used to store keys, etc.

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

# License

[`AGPL-3.0-or-later`](./LICENSE)
