const { describe } = require('tape-plus')
const crypto = require('@coboxcoop/crypto')
const collect = require('collect-stream')
const debug = require('@coboxcoop/logger')('@coboxcoop/space')
const randomWords = require('random-words')
const sinon = require('sinon')
const proxyquire = require('proxyquire')
const { Header } = require('hypertrie/lib/messages')
const Drive = require('@coboxcoop/drive')
const { setupLevel } = require('@coboxcoop/replicator/lib/level')
const path = require('path')
const Corestore = require('corestore')
const RAM = require('random-access-memory')
const Multifeed = require('@coboxcoop/multifeed')
const Kappa = require('kappa-core')
const memdb = require('level-mem')
const { tryDecode } = require('kappa-drive')

const Handler = require('../lib/handlers/state')

const { tmp, cleanup } = require('./util')

describe('@coboxcoop/space: StateHandler', (context) => {
  context('constructor()', (assert, next) => {
    var storage = tmp()
    var address = crypto.address()
    var deriveKeyPair = crypto.keyPair.bind(null, crypto.masterKey())
    var corestore = new Corestore(RAM)
    var core = new Kappa()
    var feeds = new Multifeed(corestore, { rootKey: address })
    var db = memdb()
    var drive = Drive({ corestore, address, feeds, core, db, deriveKeyPair })

    var handler = Handler({
      db,
      drive,
      core,
      feeds
    })

    assert.ok(handler.core instanceof Kappa, 'sets this.core')
    assert.ok(handler.feeds, 'sets this.feeds')
    assert.same(handler.drive, drive, 'sets this.drive')
    assert.ok(handler.db, 'sets this.db')

    cleanup(storage, next)
  })

  context('ready()', (assert, next) => {
    var storage = tmp()
    var address = crypto.address()
    var parentKey = crypto.masterKey()
    var deriveKeyPair = crypto.keyPair.bind(null, parentKey)
    var core = new Kappa()
    var corestore = new Corestore(RAM, { parentKey })
    var feeds = new Multifeed(corestore, { rootKey: address })
    var db = memdb()
    var drive = Drive({ corestore, address, feeds, core, db, deriveKeyPair })

    var handler = Handler({
      db,
      drive,
      core,
      feeds
    })

    handler.ready(() => {
      assert.same(handler._feed, handler.drive.metadata, 'is the drive metadata feed')
      assert.ok(handler.core.view.state, 'creates a view')
      assert.same(handler.read, handler.core.view.state.read, 'binds view.read() to read()')

      drive.writeFile('./woof.txt', 'woof', (err) => {
        assert.error(err, 'no error')

        handler._feed.get(0, (err, msg) => {
          assert.error(err, 'no error')
          var header = Header.decode(msg)

          assert.same(header.type, 'hypertrie', 'is a hypertrie')
          cleanup(storage, next)
        })
      })
    })
  })

  context('read()', (assert, next) => {
    var storage = tmp()
    var address = crypto.address()
    var deriveKeyPair = crypto.keyPair.bind(null, crypto.masterKey())
    var core = new Kappa()
    var corestore = new Corestore(RAM)
    var feeds = new Multifeed(corestore, { rootKey: address })
    var db = memdb()
    var drive = Drive({ corestore, address, feeds, core, db, deriveKeyPair })

    var handler = Handler({
      db,
      drive,
      core,
      feeds
    })

    handler.ready(() => {
      drive.writeFile('/hello.txt', 'world', (err) => {
        assert.error(err, 'no error')

        handler._feed.get(2, (err, msg) => {
          assert.error(err, 'no error')
          var check = [{ key: handler._feed.key.toString('hex'), seq: 2, value: tryDecode(msg) }]

          handler.ready(() => {
            collect(handler.read({ query: [{ $filter: { value: { timestamp: { $gt: 0 } } } }] }), (err, msgs) => {
              assert.error(err, 'no error')
              assert.same(msgs.length, 1, 'gets one message')
              assert.same(msgs, check, 'message matches')
              cleanup(storage, next)
            })
          })
        })
      })
    })
  })
})
